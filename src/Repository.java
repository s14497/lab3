import java.util.List;
import java.sql.Connection;

public interface Repository<Repo> {

	public List<Repo> getAll();
	public int add(Repo repo);
	public void clear();
	public Connection getConnection();
}
